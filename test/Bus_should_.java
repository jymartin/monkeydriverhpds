import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class Bus_should_ {

    private Bus bus;

    @Before
    public void setUp() {
        bus = new MyBus();
    }

    @Test
    public void send_a_message_to_subscriber() {
        BusSubscriber subscriber = mock(BusSubscriber.class);
        Message message = mock(Message.class);
        String messageType = "foo";
        when(message.type()).thenReturn(messageType);
        bus.subscribe(subscriber, messageType);

        bus.send(message);

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber, times(1)).receive(captor.capture());
        for (Message aMessage : captor.getAllValues()) {
            assertThat(aMessage.type(), is("foo"));
        }
    }

    @Test
    public void send_a_message_for_two_subscriber()  {
        BusSubscriber subscriber = mock(BusSubscriber.class);
        BusSubscriber subscriber2 = mock(BusSubscriber.class);
        Message message = mock(Message.class);
        String messageType = "foo";
        when(message.type()).thenReturn(messageType);
        bus.subscribe(subscriber, messageType);
        bus.subscribe(subscriber2, messageType);

        bus.send(message);

        verify(subscriber).receive(message);
        verify(subscriber2).receive(message);
    }

}
