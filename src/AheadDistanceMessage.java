public interface AheadDistanceMessage extends Message{
    int distance();
}
