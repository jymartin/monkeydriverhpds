public interface BusSubscriber {
    void receive(Message message);
}
